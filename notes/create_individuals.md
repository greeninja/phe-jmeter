### Creating Individual's in a loop

_In rails console_

* Create the individuals

```ruby

count = 50

count.times do |i|
  name = ""
  source = ("a".."z").to_a
  6.times{ name += source[rand(source.size)].to_s}
  
  options = {
    forename: "#{name.capitalize}", surname: 'Smith', email: '123@123.com',
    proxy: false, monitored_by_hp_zone: false, notification_route: 'A test from Lab X',
    category: :positive_test, tested_at: Date.parse('2020-06-11'),
    dead: false, high_risk: false, under_18: false, status: :auto_invited
  }

  add = Individual.find_or_create_by!(options) do |individual|
    individual.password = individual.password_confirmation = 'testing_password'
  end
end

```

* Then get it all out in CSV to use in jmeter

```bash
psql -d sampledb -t -A -F"," -c "select account_id, individuals.id, forename, surname, events.id as events_id from individuals inner join events on individuals.id=events.individual_id where account_id != '' and email = '123@123.com' and surname = 'Smith' and events.exposure_group ='Household visitor';"

```

