### Create Staff

_In rails console_



__Change Values before running!__

Had to disable mailer function in the StaffMember model (as it's running in prod) for this to work.


```ruby

phone = Rails.application.credentials.two_factor_authentication[:skip_value]

tiers = {"one"=>{"id"=>1, "count"=>200}, "two"=>{"id"=>2, "count"=>3000}, "three"=>{"id"=>3, "count"=>15000}}

tiers.each do | k,v |
  v['count'].times do |i|
    name = ""
    source = ("a".."z").to_a
    6.times{ name += source[rand(source.size)].to_s}
    
    options = {
      forename: "#{name.capitalize}", surname: k, email: "tier_#{k}_#{name}@phe.gov.uk", 
      tier: "#{v['id']}", organisation: 'PHE', phone: phone
    }

    tier_staff_member = StaffMember.find_or_create_by(options) do |staff_member|
      staff_member.password = staff_member.password_confirmation = 'testing_password'
    end
  end
end

```


Grab them back by tier from Postgres

```bash

psql -d sampledb -t -A -F"," -c "select email from staff_members where phone != '' and tier = 3;"

```
